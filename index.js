let min = document.getElementById("min");
let sec = document.getElementById("sec");
let todo = document.getElementById("todo");
let startbtn = document.getElementById("startbtn");
let logtime = document.getElementById("logtime");
let cnt = (localStorage.getItem('count') != null) ? localStorage.getItem('count') : 0;
cnt = parseInt(cnt);
let timer = 0;
let k = 0;
document.addEventListener('DOMContentLoaded', function () {
    minsec(cnt);
    if (localStorage.getItem('logs') == null) localStorage.setItem('logs', "[]");
    else {
        logs = localStorage.getItem('logs');
        logs = JSON.parse(logs);
        logs.forEach(function (log, i, arr) {

            if (k < log.k) k = log.k;
            logtime.innerHTML = logtime.innerHTML + ("<div id='log" + log.k + "'><div class='d-flex justify-content-between p-1 border mb-1'>" +
                log.time + "<button onclick='deletelog(" + log.k + ")' class='btn btn-danger btn-sm'>X</button></div></div>");
        });
    }
});

function deletelog(idlog) {
    let log2 = [];
    logs = localStorage.getItem('logs');
    logs = JSON.parse(logs);
    logtime.innerHTML = "";
    logs.forEach(function (log, i, arr) {
        console.log(log.k, idlog);
        if (log.k != idlog) {

            log2.push({
                "k": log.k,
                "time": log.time
            });
            if (k < log.k) k = log.k;
            logtime.innerHTML = logtime.innerHTML + ("<div id='log" + log.k + "'><div class='d-flex justify-content-between p-1 border mb-1'>" +
                log.time + "<button onclick='deletelog(" + log.k + ")' class='btn btn-danger btn-sm'>X</button></div></div>");
        }
    });
    localStorage.setItem('logs', JSON.stringify(log2));
}

function counter(action) {
    if (action == 1 && timer == 0) {
        startbtn.innerHTML = "Pause";
        startbtn.className = "btn btn-primary";
        timer = setInterval(function () {
            cnt++;
            minsec(cnt);
        }, 1000);

    } else if (action == 1 && timer != 0) {
        startbtn.innerHTML = "Start";
        startbtn.className = "btn btn-secondary";
        localStorage.setItem('count', (parseInt(min.innerHTML) * 60) + parseInt(sec.innerHTML));
        clearInterval(timer);
        timer = 0;
    }
    if (action == 2) {
        localStorage.setItem('count', 0);
        localStorage.setItem('logs', "[]");
        logtime.innerHTML = "";
        k = 0;
        cnt = 0;
        minsec(0);
        clearInterval(timer);
        timer = 0;
    }
    if (action == 0) {
        logs = localStorage.getItem('logs');
        logs = JSON.parse(logs);
        //logs.push(3);
        k++;
        logs.push({
            "k": k,
            "time": min.innerHTML + ":" + sec.innerHTML
        });
        localStorage.setItem('logs', JSON.stringify(logs));
        logtime.innerHTML = logtime.innerHTML + ("<div id='log" + k + "'><div class='d-flex justify-content-between p-1 border mb-1'>" +
            min.innerHTML + ":" + sec.innerHTML + "<button onclick='deletelog(" + k + ")' class='btn btn-danger btn-sm'>X</button></div></div>");
    }

}

function minsec(s) {
    if (parseInt(s / 60) < 10) min.innerHTML = "0" + parseInt(s / 60);
    else min.innerHTML = parseInt(s / 60);
    if ((s % 60) < 10) sec.innerHTML = "0" + s % 60;
    else sec.innerHTML = s % 60;
}